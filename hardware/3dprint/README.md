![3d print](3dprint.jpg)

This folder contains STL files which you can use to make a 3D printed 
enclosure for Papyr. We used an SLA process for the above. Brass threaded inserts 
are needed for the M2 screws.
