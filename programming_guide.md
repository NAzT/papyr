 
## Getting started

You can develop your custom application and upload your firmware on Papyr. We have explained below the steps to do this with Zephyr RTOS and native nRF5 SDK.

Papyr can be reprogrammed using SWD header. You can use our inexpensive Bumpy SWD debugger and PogoProg Model B adapter for that purpose or you can also use nRF5 DK to program. Here are the instructions for programming Papyr.

### Using Zephyr

  1. First, you have to set up the Zephyr development environment. Start here [Zephyr getting started guide](https://docs.zephyrproject.org/latest/getting_started/getting_started.html).
  2. Clone [Papyr](https://gitlab.com/electronutlabs-public/papyr) repository in your system.
  3. The lastest version of zephyr supports Papyr, but if you cannot find *nrf52840_papyr* inside *zephyr/boards/arm/*, then copy Papyr board files to **$ZEPHYR_BASE/boards/arm/**. The board files can be found inside *nrf52840_papyr* folder in the top level Papyr repo. 

Papyr repository contains basic examples which will help you getting started with development on Zephyr. 

**Build and flash Hello Papyr! example on Zephyr**

In this example the string **Hello Papyr** will be displayed on the screen. Go to the example folder and follow these steps.
```
$ mkdir build && cd build

# Use cmake to configure a Ninja-based build system:
$ cmake -GNinja -DBOARD=nrf52840_papyr ..

# Now run ninja on the generated build system:
$ ninja

```
To flash the firmware, you will need bumpy and PogoProg. If you have purchased Papyr dev kit it will contain both of them or otherwise you can buy them at [tindie](https://www.tindie.com/stores/ElectronutLabs/).

Connect Bumpy with PogoProg.

| Bumpy | PogoProg |
|----------|-------|
| 3.3 | VDD|
| GND | GND|
| SWCLK | SWCLK|
| SWDIO | SWDIO|

Touch and press the respective PogoProg pins to SWD pads of Papyr like shown below.  


![](papyr-prog.jpg)


Run the following command. 

```
$ ninja flash
```

If the steps were carried out accordingly, you will see the output below. 

![](code/zephyr/hello_papyr/hello_papyr.jpg)

**Note :** When flashing your code, the command *ninja flash* uses black magic probe as runner. But, if you wish to use JLINK, follow the directions given below.

**Flash using JLINK as runner**

The the default runner for the board is chosen from the *board.cmake* file inside *zephyr/boards/arm/nrf52840_papyr/* directory. Delete the existing lines and copy the following lines inside *board.cmake* file to change the default runner to JLINK.

```
board_runner_args(nrfjprog "--nrf-family=NRF52")
board_runner_args(jlink "--device=nrf52" "--speed=4000")
include(${ZEPHYR_BASE}/boards/common/nrfjprog.board.cmake)
include(${ZEPHYR_BASE}/boards/common/jlink.board.cmake)
```
Now, go back to your project, compile using cmake and do ninja flash. The runner chosen will be JLINK this time. 

If you want to change it back to *black magic probe* then copy the following inside *board.cmake* file inside *zephyr/boards/arm/nrf52840_papyr/* directory.

```
include(${ZEPHYR_BASE}/boards/common/blackmagicprobe.board.cmake)
```
Now, black magic probe will be chosen as default runner.

### Using nRF5 SDK

Set up development environment for nRF5 15.2 SDK from [here](https://www.nordicsemi.com/DocLib/Content/SDK_Doc/nRF5_SDK/v15-2-0/index). Once you are done installing toolchain, SDK, GDB and other tools depending upon your system (Linux or Windows) you can run the examples inside *nrf5_SDK* in Papyr repo.

**Build and flash Hello Papyr! example with nRF5 SDK**

1. Copy the example to *nRF5_SDK_15.2.0_9412b96/examples/* folder.
2. Open terminal and navigate to *pca10056/s140/armgcc/*
3. Issue *make* command to compile the example.

```
make
```
4. You can flash the firmware either using nRF52840-DK or using bumpy. Here are the steps for both.

**Upload firmware with nRF52840-DK**

| DK | PAPYR |
|----------|-------|
| VTG | VDD|
| GND DETECT | GND|
| SWCLK | SWCLK|
| SWDIO | SWDIO|

then upload the code using the below command.
```
make flash
```
If done as directed, you will see **Hello Papyr!** message displayed on papyr board. 

**Upload firmware with bumpy**

Connect bumpy and PogoProg as directed in programming with Zephyr section above and append below code snippet to your Makefile.

```
# specify your comm port
$export BMP_PORT /dev/ttyACMx

# Flash softdevice
flash_softdevice_bmp:
    @echo Flashing: s140_nrf52_6.1.0_softdevice.hex
    arm-none-eabi-gdb -q -ex 'target extended-remote $(BMP_PORT)' -ex 'monitor swdp_scan' -ex 'attach 1' -ex 'load' -ex 'mon hard_srst' -ex 'detach' -ex 'quit' -ex 'kill' $(SDK_ROOT)/components/softdevice/s140/hex/s140_nrf52_6.1.0_softdevice.hex

# Flash the program
flash_bmp: $(OUTPUT_DIRECTORY)/nrf52840_xxaa.out
    @echo Flashing: $<
    arm-none-eabi-gdb -ex 'target extended-remote $(BMP_PORT)' -ex 'monitor swdp_scan' -ex 'attach 1' -ex 'load' -ex 'mon hard_srst' -ex 'set remote exec-file $<' -ex 'run' -ex 'quit' -ex 'kill' $<

erase_bmp:
    arm-none-eabi-gdb -q -ex 'target extended-remote $(BMP_PORT)' -ex 'monitor swdp_scan' -ex 'attach 1' -ex 'mon erase_mass' -ex 'mon hard_srst' -ex 'detach' -ex 'quit'
```
To flash the firmware, run command. 

```
 make flash_bmp
```
If the flash is successful, you will see **Hello Papyr !** displayed on your board. 

### Upload hex file 

If you are just looking to upload your firmware hex file to Papyr, you can do it via GDB. 

Plug in bumpy via USB and open the terminal. Run **arm-none-eabi-gdb**
```
~ $ arm-none-eabi-gdb
GNU gdb (GNU Tools for ARM Embedded Processors) 7.8.0.20150604-cvs
Copyright (C) 2014 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "--host=i686-linux-gnu --target=arm-none-eabi".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word".
(gdb) 
```
Now you need to connect to the target. The Black Magic Probe hardware will
create two COM ports, and you need to connect to the first one for debugging.
You can see the ports in the *Device Manager* on windows and for Linux do **$ ls /dev/ttyACM***

```
(gdb) target extended-remote COM5
Remote debugging using COM5
```

**Note**

On **Windows**, for ports >= `COM10`, add the prefix `\\.\`.

For example:

```
(gdb) target extended-remote COM3
(gdb) target extended-remote \\.\COM10
```
On **Linux** just specify the port
```
Type "apropos word" to search for commands related to "word".
(gdb) target extended-remote /dev/ttyACM0
Remote debugging using /dev/ttyACM0
(gdb) 
```
Now, we scan for SWD targets.

```
(gdb) mon swdp_scan
Target voltage: unknown
Available Targets:
No. Att Driver
 1      Nordic nRF52
 2      Nordic nRF52 Access Port
(gdb) 
```

You can see above, that it found our nRF52 target. Connect to Nordic nRF52.

```
(gdb) attach 1
Attaching to Remote target
warning: No executable has been specified and target does not support
determining executable automatically.  Try using the "file" command.
0x0002099e in ?? ()
(gdb)
```

Now upload some code to by specifying the path to your hex file.

```
(gdb) load /pca10056/s140/armgcc/_build/nrf52840_xxaa.hex
Reading symbols from blinky.hex...(no debugging symbols found)...done.
```

Now let's run the program.

```
(gdb) run
Starting program: 
```

Connect Bumpy with PogoProg. Touch and press the respective PogoProg pins to SWD pads of Papyr like shown above. And, your firmware will be flashed. 

We have covered all the basics needed to start development on Papyr. Hope to see some creative development ideas with Papyr.   



