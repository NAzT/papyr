/** @file epaper.h
 *
 * @brief Module to control epaper device
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2018 Electronut Labs.
 * All rights reserved. 
*/
 
#ifndef _EPAPER_H
#define _EPAPER_H
 
#ifdef __cplusplus 
extern "C" { 
#endif

#include "epaper_driver/epd1in54b.h"
#include "epaper_driver/epdpaint.h"

extern EPD epd;
extern unsigned char *frame_buffer_black;
extern unsigned char *frame_buffer_red;
extern Paint paint_black;
extern Paint paint_red;

/**
 * @brief Initialize epaper display
 * 
 * @return int 
 */
int epaper_init();

/**
 * @brief tests epaper
 * 
 */
void epaper_test();

void epaper_clear();
 
#ifdef __cplusplus 
}
#endif

#endif