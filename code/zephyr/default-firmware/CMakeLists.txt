cmake_minimum_required(VERSION 3.8.2)
include($ENV{ZEPHYR_BASE}/cmake/app/boilerplate.cmake NO_POLICY_SCOPE)
project(ble_with_epaper)

FILE(GLOB epaper src/epaper_driver/*.c)
FILE(GLOB fonts src/epaper_driver/fonts/*.c)

target_sources(app PRIVATE
  src/main.c
  ${epaper}
  ${fonts}
)

include_directories(src/epaper_driver)
include_directories(src/epaper_driver/fonts)
zephyr_library_include_directories($ENV{ZEPHYR_BASE}/samples/bluetooth)
