/** @file serial_process.c
 *
 * @brief Handles serial data processing
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/

#include "nrf_drv_usbd.h"
#include "app_usbd_cdc_acm.h"
#include "serial_process.h"
#include <stdbool.h>
#include "nrf_delay.h"
#include "bsp.h"

#include "nrf_calendar.h"
#include "nrf_drv_spi.h"
#include "nrf_gpio.h"
#include "epdif.h"
#include "epaper.h"
#include "ugui.h"
#include "time_backup.h"

#define PIN_E_INK 11
#define RECEIVE_TIME_STRING_LENGTH 25

extern bool time_is_set;
extern struct tm prev_time;
extern void epaper_update_calender();
extern void epaper_pins_off();
extern void update_time_in_flash();

static char m_rx_buffer[READ_SIZE];
char m_tx_buffer[NRF_DRV_USBD_EPSIZE];

volatile char rx_data;
volatile bool rx_done_evt;

/**
 * @brief CDC_ACM class instance
 * */
APP_USBD_CDC_ACM_GLOBAL_DEF(m_app_cdc_acm,
                            cdc_acm_user_ev_handler,
                            CDC_ACM_COMM_INTERFACE,
                            CDC_ACM_DATA_INTERFACE,
                            CDC_ACM_COMM_EPIN,
                            CDC_ACM_DATA_EPIN,
                            CDC_ACM_DATA_EPOUT,
                            APP_USBD_CDC_COMM_PROTOCOL_AT_V250
);

void print_current_time()
{
    nrf_delay_ms(100);

    size_t size = sprintf(m_tx_buffer, "Uncalibrated time:\t%s\r\n", nrf_cal_get_time_string(false));
    ret_code_t ret = app_usbd_cdc_acm_write(&m_app_cdc_acm, m_tx_buffer, size);
    if (ret != NRF_SUCCESS)
    {
        NRF_LOG_INFO("CDC ACM unavailable, data received: %s", m_tx_buffer);
    }

    size = sprintf(m_tx_buffer, "Calibrated time:\t%s\r\n\n", nrf_cal_get_time_string(true));
    ret = app_usbd_cdc_acm_write(&m_app_cdc_acm, m_tx_buffer, size);
    if (ret != NRF_SUCCESS)
    {
        NRF_LOG_INFO("CDC ACM unavailable, data received: %s", m_tx_buffer);
    }

    // NRF_LOG_INFO("Uncalibrated time:\t%s", nrf_cal_get_time_string(false));
    // NRF_LOG_INFO("Calibrated time:\t%s", nrf_cal_get_time_string(true));
}

// Initialize USBD peripheral
void usbd_init(void)
{
    ret_code_t ret;
    static const app_usbd_config_t usbd_config = {
        .ev_state_proc = usbd_user_ev_handler
    };
    
    ret = app_usbd_init(&usbd_config);
    APP_ERROR_CHECK(ret);

    app_usbd_class_inst_t const * class_cdc_acm = app_usbd_cdc_acm_class_inst_get(&m_app_cdc_acm);
    ret = app_usbd_class_append(class_cdc_acm);
    APP_ERROR_CHECK(ret);

    if (USBD_POWER_DETECTION)
    {
        ret = app_usbd_power_events_enable();
        APP_ERROR_CHECK(ret);
    }
    else
    {
        NRF_LOG_INFO("No USB power detection enabled\r\nStarting USB now");

        app_usbd_enable();
        app_usbd_start();
    }

    /* Give some time for the host to enumerate and connect to the USB CDC port */
    nrf_delay_ms(1000);

    bsp_board_init(BSP_INIT_LEDS);

    app_usbd_serial_num_generate();
}

/**
 * @brief User event handler @ref app_usbd_cdc_acm_user_ev_handler_t
 * */
void cdc_acm_user_ev_handler(app_usbd_class_inst_t const * p_inst,
                                    app_usbd_cdc_acm_user_event_t event)
{
    app_usbd_cdc_acm_t const * p_cdc_acm = app_usbd_cdc_acm_class_get(p_inst);

    switch (event)
    {
        case APP_USBD_CDC_ACM_USER_EVT_PORT_OPEN:
        {
            /*Setup first transfer*/
            ret_code_t ret = app_usbd_cdc_acm_read(&m_app_cdc_acm,
                                                   m_rx_buffer,
                                                   READ_SIZE);
            UNUSED_VARIABLE(ret);

            bsp_board_led_on(LED_CDC_ACM_OPEN);
            NRF_LOG_INFO("CDC ACM port opened");
            break;
        }
        case APP_USBD_CDC_ACM_USER_EVT_PORT_CLOSE:
            bsp_board_led_off(LED_CDC_ACM_OPEN);
            NRF_LOG_INFO("CDC ACM port closed");
            break;
        case APP_USBD_CDC_ACM_USER_EVT_TX_DONE:
            bsp_board_led_invert(LED_CDC_ACM_TX);
            break;
        case APP_USBD_CDC_ACM_USER_EVT_RX_DONE:
        {
            rx_done_evt = true;

            bsp_board_led_invert(LED_CDC_ACM_RX);
            break;
        }
        default:
            break;
    }
}

void usbd_user_ev_handler(app_usbd_event_type_t event)
{
    switch (event)
    {
        case APP_USBD_EVT_DRV_SUSPEND:
            break;

        case APP_USBD_EVT_DRV_RESUME:
            break;

        case APP_USBD_EVT_STARTED:
            break;

        case APP_USBD_EVT_STOPPED:
            app_usbd_disable();
            bsp_board_leds_off();
            break;

        case APP_USBD_EVT_POWER_DETECTED:
            NRF_LOG_INFO("USB power detected");

            if (!nrf_drv_usbd_is_enabled())
            {
                app_usbd_enable();
            }
            break;

        case APP_USBD_EVT_POWER_REMOVED:
        {
            NRF_LOG_INFO("USB power removed");
            app_usbd_stop();
        }
            break;

        case APP_USBD_EVT_POWER_READY:
        {
            NRF_LOG_INFO("USB ready");
            app_usbd_start();
        }
            break;

        default:
            break;
    }
}

// Process calendar time set/get commands
void serial_process()
{
    static volatile bool receive_time = false;
    static char receive_time_string[RECEIVE_TIME_STRING_LENGTH];
    static uint8_t receive_time_string_index = 0;

    if(rx_done_evt)
    {
        app_usbd_class_inst_t const * class_cdc_acm = app_usbd_cdc_acm_class_inst_get(&m_app_cdc_acm);
        app_usbd_cdc_acm_t const * p_cdc_acm = app_usbd_cdc_acm_class_get(class_cdc_acm);
        ret_code_t ret;

        do
        {
            /*Get amount of data transfered*/
            size_t size = app_usbd_cdc_acm_rx_size(p_cdc_acm);
            // NRF_LOG_INFO("RX: size: %lu char: %c", size, m_rx_buffer[0]);
            // NRF_LOG_INFO("%c", m_rx_buffer[0]);

            /* Fetch data until internal buffer is empty */
            ret = app_usbd_cdc_acm_read(&m_app_cdc_acm,
                                        m_rx_buffer,
                                        READ_SIZE);
        } while (ret == NRF_SUCCESS);

        rx_data = m_rx_buffer[0];

        size_t size;

        // Write back
        if (rx_data == '\n' || rx_data == '\r') {
            size = sprintf(m_tx_buffer, "%c\r\n", rx_data);
            NRF_LOG_RAW_INFO("%c\r\n", m_rx_buffer[0]);
        }
        else
        {
            size = sprintf(m_tx_buffer, "%c", rx_data);
            NRF_LOG_RAW_INFO("%c", m_rx_buffer[0]);
        }
        
        ret = app_usbd_cdc_acm_write(&m_app_cdc_acm, m_tx_buffer, size);
        if (ret != NRF_SUCCESS)
        {
            NRF_LOG_INFO("CDC ACM unavailable, data received: %s", m_tx_buffer);
        }

        rx_done_evt = false;

        nrf_delay_ms(100);

        if (rx_data == 's' || rx_data == 'S') {
            size_t size = sprintf(m_tx_buffer, "%s", " -> Set RTC time e.g. \"2019-1-31 19:25:30\"\r\nInput: ");
            ret = app_usbd_cdc_acm_write(&m_app_cdc_acm, m_tx_buffer, size);
            if (ret != NRF_SUCCESS)
            {
                NRF_LOG_INFO("CDC ACM unavailable, data received: %s", m_tx_buffer);
            }

            receive_time = true;
            time_is_set = false;
            receive_time_string_index = 0;
        }
        else if (rx_data == 'h' || rx_data == 'H') {
            size_t size = sprintf(m_tx_buffer, "%s\r\n", " -> Help\r\nPapyr Calendar\r\n\ns - Set time\r\ng - Get time\r\n\n");
            ret = app_usbd_cdc_acm_write(&m_app_cdc_acm, m_tx_buffer, size);
            if (ret != NRF_SUCCESS)
            {
                NRF_LOG_INFO("CDC ACM unavailable, data received: %s", m_tx_buffer);
            }
        }
        else if (rx_data == 'g' || rx_data == 'G') {
            receive_time = false;
            receive_time_string_index = 0;

            size_t size = sprintf(m_tx_buffer, "%s", " -> Displaying RTC time\r\n");
            ret = app_usbd_cdc_acm_write(&m_app_cdc_acm, m_tx_buffer, size);
            if (ret != NRF_SUCCESS)
            {
                NRF_LOG_INFO("CDC ACM unavailable, data received: %s", m_tx_buffer);
            }
            print_current_time();
        }
        else if (receive_time) {
            if (rx_data == '\n' || rx_data == '\r') {
                receive_time_string[receive_time_string_index] = '\0';

                char *token;
                token = strtok(receive_time_string, "- :");

                char *time_array[6];
                uint8_t i = 0;
                while (token != NULL) {
                    time_array[i++] = token;
                    token = strtok(NULL, "- :");
                }

                uint32_t year, month, day, hour, minute, second;

                year = atoi(time_array[0]);
                month = atoi(time_array[1]);
                day = atoi(time_array[2]);
                hour = atoi(time_array[3]);
                minute = atoi(time_array[4]);
                second = atoi(time_array[5]);

                nrf_cal_set_time(year, month, day, hour, minute, second);

                size_t size = sprintf(m_tx_buffer, "Time set: %s\r\n", nrf_cal_get_time_string(false));
                ret = app_usbd_cdc_acm_write(&m_app_cdc_acm, m_tx_buffer, size);
                if (ret != NRF_SUCCESS)
                {
                    NRF_LOG_INFO("CDC ACM unavailable, data received: %s", m_tx_buffer);
                }

                memcpy(&prev_time, nrf_cal_get_time(), sizeof(struct tm));

                time_is_set = true;

                // turn on epaper display MOSFET
                nrf_gpio_pin_clear(PIN_E_INK);

                // update calendar image
                epaper_init();
                epaper_update_calender();
                epaper_uninit();

                // turn off epaper display MOSFET
                nrf_gpio_pin_set(PIN_E_INK);
                epaper_pins_off();

                update_time_in_flash();

                receive_time = false;
            }

            receive_time_string[receive_time_string_index] = rx_data;
            receive_time_string_index++;
        }
    }
}
