/** @file main.c
 *
 * @brief Papyr calender example
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "nrf.h"
#include "nrf_delay.h"
#include "app_error.h"
#include "bsp.h"
#include "nrf_calendar.h"
#include "app_uart.h"
#include "nrf_drv_spi.h"
#include "nrf_gpio.h"
#include "epdif.h"
#include "epaper.h"
#include "ugui.h"
#include "time_backup.h"

#include "nrf_drv_clock.h"
#include "app_timer.h"
#include "nrf_drv_usbd.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_power.h"

#include "serial_process.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define C_RED_BLACK C_BLUE	
#define C_RED C_GRAY	

#define PIN_E_INK 11

// #define TEST

UG_GUI ugui;

// epaper pins
EPAPER_pins epaper_pins = {
    .pinBUSY = 3,
    .pinRST = 2,
    .pinDC = 28,
    .pinSpiCS = 30,
    .pinSpiSCK = 31,
    .pinSpiMOSI = 29,
};

// Pull down epaper pins to save power
void epaper_pins_off()
{
    nrf_gpio_cfg_output(epaper_pins.pinBUSY);
    nrf_gpio_cfg_output(epaper_pins.pinRST);
    nrf_gpio_cfg_output(epaper_pins.pinDC);
    nrf_gpio_cfg_output(epaper_pins.pinSpiCS);
    nrf_gpio_cfg_output(epaper_pins.pinSpiSCK);
    nrf_gpio_cfg_output(epaper_pins.pinSpiMOSI);

    nrf_gpio_pin_clear(epaper_pins.pinBUSY);
    nrf_gpio_pin_clear(epaper_pins.pinRST);
    nrf_gpio_pin_clear(epaper_pins.pinDC);
    nrf_gpio_pin_clear(epaper_pins.pinSpiCS);
    nrf_gpio_pin_clear(epaper_pins.pinSpiSCK);
    nrf_gpio_pin_clear(epaper_pins.pinSpiMOSI);
}

// Display Calendar image
void epaper_display_initial_image()
{
    Paint_Init(&paint_black, frame_buffer_black, epd.width, epd.height);
    Paint_Init(&paint_red, frame_buffer_red, epd.width, epd.height);
    Paint_Clear(&paint_black, UNCOLORED);
    Paint_Clear(&paint_red, UNCOLORED);
    
    Paint_SetRotate(&paint_red, 3);
    Paint_DrawStringAt(&paint_red, 23, 40, "p a p y r", &Font24, COLORED);
    Paint_DrawStringAt(&paint_red, 32, 80, "calendar", &Font24, COLORED);

    EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);
}

// map pixels with coordinates and color to actual buffer
void putpixel(int16_t x, int16_t y, uint8_t color)
{
    switch(color)
    {
        case C_WHITE:
            Paint_DrawPixel(&paint_black, x, y, UNCOLORED);
            Paint_DrawPixel(&paint_red, x, y, UNCOLORED);
            break;
        case C_RED_BLACK:
            Paint_DrawPixel(&paint_black, x, y, COLORED);
            Paint_DrawPixel(&paint_red, x, y, COLORED);
            break;
        case C_RED:
            Paint_DrawPixel(&paint_red, x, y, COLORED);
            break;
        case C_BLACK:
            Paint_DrawPixel(&paint_black, x, y, COLORED);
            break;
    }
}

struct tm prev_time;

// Update Calendar image
void epaper_update_calender()
{
    // get current time
    struct tm *current_time;
    current_time = nrf_cal_get_time();

    UG_Init(&ugui, putpixel, epd.width, epd.height);

    Paint_Init(&paint_black, frame_buffer_black, epd.width, epd.height);
    Paint_Init(&paint_red, frame_buffer_red, epd.width, epd.height);
    Paint_Clear(&paint_black, UNCOLORED);
    Paint_Clear(&paint_red, UNCOLORED);
    Paint_SetRotate(&paint_red, 3);
    Paint_SetRotate(&paint_black, 3);

    Paint *paint_theme;
    if(current_time->tm_wday == 0) // Sunday
    {
        paint_theme = &paint_red;
    }
    else
    {
        paint_theme = &paint_black;
    }
    
    Paint_DrawFilledRectangle(paint_theme, 0, 0, 200, 70, COLORED);

    char year_string[6];
    int year_string_len = snprintf(year_string, sizeof(year_string), "%d", current_time->tm_year + 1900);
    Paint_DrawStringAt(paint_theme, (200-(year_string_len*(Font24.Width)))/2, 10, year_string, &Font24, UNCOLORED);

    int month_string_len = strlen(month_string[current_time->tm_mon]);
    Paint_DrawStringAt(paint_theme, (200-(month_string_len*(Font24.Width)))/2, 40, month_string[current_time->tm_mon], &Font24, UNCOLORED);
    
    char date_string[3];
    int date_string_len = snprintf(date_string, sizeof(date_string), "%d", current_time->tm_mday);
    UG_FontSelect(&FONT_32X53);
    UG_SetForecolor(C_BLACK);
    UG_PutString((200-(date_string_len*(FONT_32X53.char_width)))/2, 100, date_string);

    Paint_DrawStringAt(paint_theme, 140, 170, week_day_string[current_time->tm_wday], &Font24, COLORED);
    
    #ifdef TEST
    Paint_DrawStringAt(paint_theme, 10, 180, (nrf_cal_get_time_string(false) + 9), &Font16, COLORED);
    #endif

    EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);
}

bool time_is_set = false;

// update timestamp in flash every 5 minutes, on power reset papyr will not lose timestamp
void update_time_in_flash()
{
    if (time_is_set)
    {
        // get current time
        struct tm *current_time;
        current_time = nrf_cal_get_time();

        // print_current_time();

        write_time_to_flash(current_time);
    }

    if(time_is_set)
    {
        struct tm *current_time;
        current_time = nrf_cal_get_time();

        if(current_time->tm_mday != prev_time.tm_mday)
        {
            // turn on epaper display MOSFET
            nrf_gpio_pin_clear(PIN_E_INK);
            
            // update calendar image
            epaper_init();
            epaper_update_calender();
            epaper_uninit();

            // turn off epaper display MOSFET
            nrf_gpio_pin_set(PIN_E_INK);
            epaper_pins_off();
        }

        memcpy(&prev_time, current_time, sizeof(struct tm));
    }
}

/** @brief Function for initializing the nrf_log module. */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

// Main function
int main(void)
{
    log_init();
    NRF_LOG_INFO("Starting...");
    nrf_delay_ms(1000);

    ret_code_t ret;

    ret = nrf_drv_clock_init();
    APP_ERROR_CHECK(ret);

    usbd_init();
    NRF_LOG_INFO("USBD UART example started.");

    // turn off epaper display MOSFET
    nrf_gpio_cfg_output(PIN_E_INK);
    nrf_gpio_pin_set(PIN_E_INK);
    
    // Turn off e-Paper pins
    epaper_pins_off();

    NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
    NRF_CLOCK->TASKS_HFCLKSTART = 1;
    while(NRF_CLOCK->EVENTS_HFCLKSTARTED == 0);

    nrf_cal_init();
    nrf_cal_set_callback(update_time_in_flash, 300); // update timestamp in flash every 5 minutes

    /* Register first to receive an event when initialization is complete. */
    (void) fds_register(fds_evt_handler);

    NRF_LOG_INFO("Initializing fds...");

    ret_code_t rc = fds_init();
    APP_ERROR_CHECK(rc);

    /* Wait for fds to initialize. */
    wait_for_fds_ready();

    fds_stat_t stat = {0};

    rc = fds_stat(&stat);
    APP_ERROR_CHECK(rc);

    NRF_LOG_INFO("Found %d valid records.", stat.valid_records);
    NRF_LOG_INFO("Found %d dirty records (ready to be garbage collected).", stat.dirty_records);

    rc = fds_gc();

    if(rc != FDS_SUCCESS)
    {
        NRF_LOG_ERROR("error: garbage collection returned %s\r\n", fds_err_str[rc]);
    }

    nrf_delay_ms(100);
    struct tm current_time;

    if(read_time_from_flash(&current_time))
    {
        NRF_LOG_INFO("Timestamp found in flash\r\nUpdating e-Paper display...");
        NRF_LOG_INFO("tm_sec:\t%d\r\n"
                "tm_min:\t%d\r\n"
                "tm_hour:\t%d\r\n"
                "tm_mday:\t%d\r\n"
                "tm_mon:\t%d\r\n"
                "tm_year:\t%d\r\n",
                        current_time.tm_sec,
                        current_time.tm_min,
                        current_time.tm_hour,
                        current_time.tm_mday,
                        current_time.tm_mon,
                        current_time.tm_year);

        nrf_cal_set_time(current_time.tm_year + 1900, current_time.tm_mon + 1, 
                            current_time.tm_mday, current_time.tm_hour, 
                            current_time.tm_min, current_time.tm_sec);

        NRF_LOG_INFO("Time set: ");
        NRF_LOG_INFO("%s", nrf_cal_get_time_string(false));

        memcpy(&prev_time, nrf_cal_get_time(), sizeof(struct tm));
        
        nrf_delay_ms(100);

        time_is_set = true;

        // turn on epaper display MOSFET
        nrf_gpio_pin_clear(PIN_E_INK);
        
        // update calendar image
        epaper_init();
        epaper_update_calender();
        epaper_uninit();

        // turn off epaper display MOSFET
        nrf_gpio_pin_set(PIN_E_INK);
        epaper_pins_off();

        update_time_in_flash();
    }
    else
    {
        NRF_LOG_RAW_INFO("**************************************************\r\n");
        NRF_LOG_RAW_INFO("Timestamp not found in flash\r\nPlease set the time.\r\n");
        NRF_LOG_RAW_INFO("**************************************************\r\n");

        // turn on epaper display MOSFET
        nrf_gpio_pin_clear(PIN_E_INK);

        epaper_init();
        epaper_display_initial_image();
        epaper_uninit();

        // turn off epaper display MOSFET
		nrf_gpio_pin_set(PIN_E_INK);
		epaper_pins_off();
    }

    while (true)
    {
        while (app_usbd_event_queue_process())
        {
            /* Nothing to do */
        }

        serial_process();
    }
}
