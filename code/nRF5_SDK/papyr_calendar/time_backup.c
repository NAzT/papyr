/** @file time_backup.c
 *
 * @brief Store time in flash
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/

#include <stdbool.h>
#include <string.h>
#include "time_backup.h"

/* Array to map FDS return values to strings. */
char const * fds_err_str[] =
{
    "FDS_SUCCESS",
    "FDS_ERR_OPERATION_TIMEOUT",
    "FDS_ERR_NOT_INITIALIZED",
    "FDS_ERR_UNALIGNED_ADDR",
    "FDS_ERR_INVALID_ARG",
    "FDS_ERR_NULL_ARG",
    "FDS_ERR_NO_OPEN_RECORDS",
    "FDS_ERR_NO_SPACE_IN_FLASH",
    "FDS_ERR_NO_SPACE_IN_QUEUES",
    "FDS_ERR_RECORD_TOO_LARGE",
    "FDS_ERR_NOT_FOUND",
    "FDS_ERR_NO_PAGES",
    "FDS_ERR_USER_LIMIT_REACHED",
    "FDS_ERR_CRC_CHECK_FAILED",
    "FDS_ERR_BUSY",
    "FDS_ERR_INTERNAL",
};

/* Array to map FDS events to strings. */
static char const * fds_evt_str[] =
{
    "FDS_EVT_INIT",
    "FDS_EVT_WRITE",
    "FDS_EVT_UPDATE",
    "FDS_EVT_DEL_RECORD",
    "FDS_EVT_DEL_FILE",
    "FDS_EVT_GC",
};

/* Keep track of the progress of a delete_all operation. */
static struct
{
    bool delete_next;   //!< Delete next record.
    bool pending;       //!< Waiting for an fds FDS_EVT_DEL_RECORD event, to delete the next record.
} m_delete_all;

/* Flag to check fds initialization. */
static bool volatile m_fds_initialized;

void fds_evt_handler(fds_evt_t const * p_evt)
{
    printf("Event: %s received (%s)\r\n",
                  fds_evt_str[p_evt->id],
                  fds_err_str[p_evt->result]);

    switch (p_evt->id)
    {
        case FDS_EVT_INIT:
            if (p_evt->result == FDS_SUCCESS)
            {
                m_fds_initialized = true;
            }
            break;

        case FDS_EVT_WRITE:
        {
            if (p_evt->result == FDS_SUCCESS)
            {
                printf("Record ID:\t0x%04x\r\n",  p_evt->write.record_id);
                printf("File ID:\t0x%04x\r\n",    p_evt->write.file_id);
                printf("Record key:\t0x%04x\r\n", p_evt->write.record_key);
            }
        } break;

        case FDS_EVT_DEL_RECORD:
        {
            if (p_evt->result == FDS_SUCCESS)
            {
                printf("Record ID:\t0x%04x\r\n",  p_evt->del.record_id);
                printf("File ID:\t0x%04x\r\n",    p_evt->del.file_id);
                printf("Record key:\t0x%04x\r\n", p_evt->del.record_key);
            }
            m_delete_all.pending = false;
        } break;

        default:
            break;
    }
}

// Read stored timestamp_record to tm structure
bool read_time_from_flash(struct tm *current_time)
{
    fds_record_desc_t desc = {0};
    fds_find_token_t  tok  = {0};

    if(fds_record_find(CONFIG_FILE, CONFIG_REC_KEY, &desc, &tok) == FDS_SUCCESS)
    {
        ret_code_t rc;
        fds_flash_record_t frec = {0};

        rc = fds_record_open(&desc, &frec);
        switch (rc)
        {
            case FDS_SUCCESS:
                break;

            case FDS_ERR_CRC_CHECK_FAILED:
                printf("error: CRC check failed!\r\n");
                break;

            case FDS_ERR_NOT_FOUND:
                printf("error: record not found!\r\n");
                break;

            default:
            {
                printf("error: unexpecte error %s.\r\n", fds_err_str[rc]);
                break;
            }
        }

        memcpy(current_time, frec.p_data, sizeof(struct tm));

        rc = fds_record_close(&desc);
        APP_ERROR_CHECK(rc);

        return true;
    }
    else
    {
        return false;
    }
}

// Store timestamp_record to flash
bool write_time_to_flash(struct tm *current_time)
{
    // garbage collection
    ret_code_t rc = fds_gc();

    if(rc != FDS_SUCCESS)
    {
        printf("error: garbage collection returned %s\r\n", fds_err_str[rc]);
    }

    fds_record_desc_t desc = {0};
    fds_find_token_t  ftok = {0};

    fds_record_t timestamp_record =
    {
        .file_id = CONFIG_FILE,
        .key = CONFIG_REC_KEY,
        .data.p_data = current_time,
        /* The length of a record is always expressed in 4-byte units (words). */
        .data.length_words = (sizeof(struct tm) + 3) / sizeof(uint32_t),
    };

    if (fds_record_find(CONFIG_FILE, CONFIG_REC_KEY, &desc, &ftok) == FDS_SUCCESS)
    {
        printf("Updating config file...\r\n");

        rc = fds_record_update(&desc, &timestamp_record);
        APP_ERROR_CHECK(rc);
    }
    else
    {
        printf("Writing config file...\r\n");

        rc = fds_record_write(NULL, &timestamp_record);
        APP_ERROR_CHECK(rc);
    }
}

/**@brief   Begin deleting all records, one by one. */
void delete_all_begin(void)
{
    m_delete_all.delete_next = true;
}

bool record_delete_next(void)
{
    fds_find_token_t  tok   = {0};
    fds_record_desc_t desc  = {0};

    if (fds_record_iterate(&desc, &tok) == FDS_SUCCESS)
    {
        ret_code_t rc = fds_record_delete(&desc);
        if (rc != FDS_SUCCESS)
        {
            return false;
        }

        return true;
    }
    else
    {
        /* No records left to delete. */
        return false;
    }
}

/**@brief   Process a delete all command.
 *
 * Delete records, one by one, until no records are left.
 */
void delete_all_process(void)
{
    if (   m_delete_all.delete_next
        & !m_delete_all.pending)
    {
        printf("Deleting next record.\r\n");

        m_delete_all.delete_next = record_delete_next();
        if (!m_delete_all.delete_next)
        {
            printf("No records left to delete.\r\n");
        }
    }
}

/**@brief   Wait for fds to initialize. */
void wait_for_fds_ready(void)
{
    while (!m_fds_initialized)
    {
        // power_manage();
    }
}