# Papyr Calendar Example

![papyr_calendar.jpeg](papyr_calendar.jpeg)

This example firmware imitates a **tear off date calendar**.
This uses Internal RTC Clock for keeping track of time, and storing timestamp into Flash memory so that we don't lose timestamp on reset. Papyr e-paper display refreshes once a day updating date. Time can be set over Papyr USB port, check [usage](#usage) section.

## Build and flash firmware

Set up development environment for nRF5 15.2 SDK from [here](https://www.nordicsemi.com/DocLib/Content/SDK_Doc/nRF5_SDK/v15-2-0/index). Once you are done installing toolchain, SDK, GDB and other tools depending upon your system (Linux or Windows) you can run the examples inside *nrf5_SDK* in Papyr repo.

**Build and flash Calendar example with nRF5 SDK**

1. Copy the example to *nRF5_SDK_15.2.0_9412b96/examples/* folder.
2. Open terminal and navigate to *pca10056/blank/armgcc/*
3. Issue *make* command to compile the example.

```
make
```
4. You can flash the firmware either using nRF52840-DK or using bumpy. Here are the steps for both.

**Upload firmware with nRF52840-DK**

| DK | PAPYR |
|----------|-------|
| VTG | VDD|
| GND DETECT | GND|
| SWCLK | SWCLK|
| SWDIO | SWDIO|

then upload the code using the below command.
```
make flash
```

**Upload firmware with bumpy**

Connect bumpy and Papyr according to the table below.

| BUMPY | PAPYR |
|----------|-------|
| 3V3 | VDD|
| GND | GND|
| SWCLK | SWCLK|
| SWDIO | SWDIO|

When connectiong bumpy to host machine, Bumpy will be show up as two port, One for programming and debugging, second for UART communcation.

Set BMP_PORT to programming port of bumpy:

`$export BMP_PORT /dev/ttyACMx`

To flash the firmware, run command. 

` make flash_bmp`

## Usage

Connect Papyr USB to computer, open serial terminal and set the time.

`minicom -b 115200 -D /dev/ttyACMx`

Type these commands in the serial terminal:
* h - Help
* s - Set time
* g - Get time

Time is updated every 5 minutes in non-volatile memoory so time is persisted even when papyr gets off.