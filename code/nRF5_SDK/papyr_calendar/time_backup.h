#ifndef _TIME_BACKUP_H_
#define _TIME_BACKUP_H_

#include "fds.h"
#include "time.h"

/* File ID and Key used for the configuration record. */
#define CONFIG_FILE     (0xF010)
#define CONFIG_REC_KEY  (0x7010)

extern char const * fds_err_str[];

void fds_evt_handler(fds_evt_t const * p_evt);
void wait_for_fds_ready(void);

// Read stored timestamp_record to tm structure
bool read_time_from_flash(struct tm *current_time);

// Store timestamp_record to flash
bool write_time_to_flash(struct tm *current_time);

#endif