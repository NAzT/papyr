import board
import digitalio
from PapyrFb import *

led = digitalio.DigitalInOut(board.L)
led.direction = digitalio.Direction.OUTPUT

led.value = 0 # on

pfb = PapyrFb()
epd = PapyrEpd(pfb)

pfb.rect(20, 20, pfb.width-40, pfb.height-40, RED)
pfb.rect(22, 22, pfb.width-44, pfb.height-44, BLACK)
pfb.rect(24, 24, pfb.width-48, pfb.height-48, RED)

# larger fonts coming soon
pfb.text("Hello Papyr", 67, 100, BLACK)
pfb.circle(100, 100, 38, RED)

# rings.jpg generated from follwing snippet
# for i in range(0, 100, 20):
#     pfb.circle(100, 100, i, BLACK)
#     pfb.circle(100, 100, i+4, RED)

epd.refresh()
epd.sleep()

led.value = 1 # off