# The MIT License (MIT)
#
# Copyright (c) 2019 Tavish Naruka <tavishnaruka@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""
`PapyrFb`
====================================================

CircuitPython pure-python framebuf module, based on the adafruit_framebuf module.

* Author: Tavish Naruka

Implementation Notes
--------------------

**Hardware:**

* `Electronut Labs Papyr <https://docs.electronut.in/papyr>`_

**Software and Dependencies:**

* Adafruit CircuitPython firmware for the supported boards:
  https://github.com/adafruit/circuitpython/releases

"""

__version__ = "0.0.1"

import struct
import board
import digitalio
import busio
import time
from adafruit_bus_device.spi_device import SPIDevice

WHITE = 0
BLACK = 1
RED   = 2

class PapyrEpd:
    """Papyr E-paper display driver object
    """
    PANEL_SETTING                               = 0x00
    POWER_SETTING                               = 0x01
    POWER_OFF                                   = 0x02
    POWER_OFF_SEQUENCE_SETTING                  = 0x03
    POWER_ON                                    = 0x04
    POWER_ON_MEASURE                            = 0x05
    BOOSTER_SOFT_START                          = 0x06
    DEEP_SLEEP                                  = 0x07
    DATA_START_TRANSMISSION_1                   = 0x10
    DATA_STOP                                   = 0x11
    DISPLAY_REFRESH                             = 0x12
    DATA_START_TRANSMISSION_2                   = 0x13
    PLL_CONTROL                                 = 0x30
    TEMPERATURE_SENSOR_COMMAND                  = 0x40
    TEMPERATURE_SENSOR_CALIBRATION              = 0x41
    TEMPERATURE_SENSOR_WRITE                    = 0x42
    TEMPERATURE_SENSOR_READ                     = 0x43
    VCOM_AND_DATA_INTERVAL_SETTING              = 0x50
    LOW_POWER_DETECTION                         = 0x51
    TCON_SETTING                                = 0x60
    TCON_RESOLUTION                             = 0x61
    SOURCE_AND_GATE_START_SETTING               = 0x62
    GET_STATUS                                  = 0x71
    AUTO_MEASURE_VCOM                           = 0x80
    VCOM_VALUE                                  = 0x81
    VCM_DC_SETTING_REGISTER                     = 0x82
    PROGRAM_MODE                                = 0xA0
    ACTIVE_PROGRAM                              = 0xA1
    READ_OTP_DATA                               = 0xA2

    lut_vcom0 = bytearray([
      0x0E, 0x14, 0x01, 0x0A, 0x06, 0x04, 0x0A, 0x0A,
      0x0F, 0x03, 0x03, 0x0C, 0x06, 0x0A, 0x00
    ])

    lut_w = bytearray([
      0x0E, 0x14, 0x01, 0x0A, 0x46, 0x04, 0x8A, 0x4A,
      0x0F, 0x83, 0x43, 0x0C, 0x86, 0x0A, 0x04
    ])

    lut_b =  bytearray([
      0x0E, 0x14, 0x01, 0x8A, 0x06, 0x04, 0x8A, 0x4A,
      0x0F, 0x83, 0x43, 0x0C, 0x06, 0x4A, 0x04
    ])

    lut_g1 =  bytearray([
      0x8E, 0x94, 0x01, 0x8A, 0x06, 0x04, 0x8A, 0x4A,
      0x0F, 0x83, 0x43, 0x0C, 0x06, 0x0A, 0x04
    ])

    lut_g2 =  bytearray([
      0x8E, 0x94, 0x01, 0x8A, 0x06, 0x04, 0x8A, 0x4A,
      0x0F, 0x83, 0x43, 0x0C, 0x06, 0x0A, 0x04
    ])

    lut_vcom1 =  bytearray([
      0x03, 0x1D, 0x01, 0x01, 0x08, 0x23, 0x37, 0x37,
      0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    ])

    lut_red0 =  bytearray([
      0x83, 0x5D, 0x01, 0x81, 0x48, 0x23, 0x77, 0x77,
      0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    ])

    lut_red1 =  bytearray([
      0x03, 0x1D, 0x01, 0x01, 0x08, 0x23, 0x37, 0x37,
      0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    ])

    out_lut1 = bytearray(256)
    out_lut2 = bytearray(256)

    busy = digitalio.DigitalInOut(board.BUSY)
    res = digitalio.DigitalInOut(board.RES)
    dc = digitalio.DigitalInOut(board.DC)
    eink_en = digitalio.DigitalInOut(board.EINK_EN)

    cs = digitalio.DigitalInOut(board.CS)
    comm_port = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
    device = SPIDevice(comm_port, cs)

    def __init__(self, pfb):

        # initialize lut for slightly fast draws
        buf = bytearray(1)
        for i in range(256):
            buf[0] = i
            temp = 0x00
            for bit in range(4):
                if ((buf[0] & (0x80 >> bit)) != 0):
                    temp |= 0xC0 >> (bit * 2)
            self.out_lut1[i] = temp

            temp = 0x00
            for bit in range(4, 8):
                if ((buf[0] & (0x80 >> bit)) != 0):
                    temp |= 0xC0 >> ((bit - 4) * 2)
            self.out_lut2[i] = temp
        
        self._pins_init()
        self.epd_init()
        self.pfb = pfb
        self.width = 200
        self.height = 200

        self.pfb.fill(WHITE)

    def _pins_init(self):
        self.eink_en.direction = digitalio.Direction.OUTPUT
        self.eink_en.value = 0
        time.sleep(0.5)
        self.busy.direction = digitalio.Direction.INPUT
        self.res.direction = digitalio.Direction.OUTPUT
        self.res.value = 0
        self.dc.direction = digitalio.Direction.OUTPUT
        self.dc.value = 0
        self.cs.direction = digitalio.Direction.OUTPUT
        self.cs.value = 0

    def send_cmd(self, cmd):
        self.dc.value = 0
        with self.device as bus_device:
            bus_device.write(bytes([cmd]))

    def send_data(self, data):
        self.dc.value = 1
        with self.device as bus_device:
            bus_device.write(bytes([data]))

    def wait_until_idle(self):
        while True:
            if self.busy.value != 0:
                break
            time.sleep(0.1)

    def sleep(self):
        self.send_cmd(self.VCOM_AND_DATA_INTERVAL_SETTING)
        self.send_data(0x17)
        self.send_cmd(self.VCM_DC_SETTING_REGISTER)         #to solve Vcom drop
        self.send_data(0x00)
        self.send_cmd(self.POWER_SETTING)         #power setting
        self.send_data(0x02)        #gate switch to external
        self.send_data(0x00)
        self.send_data(0x00)
        self.send_data(0x00)
        self.wait_until_idle()
        self.send_cmd(self.POWER_OFF)

    def set_lut_bw(self):
        self.send_cmd(0x20)         #g vcom
        for count in range(15):
            self.send_data(self.lut_vcom0[count])

        self.send_cmd(0x21)        #g ww --
        for count in range(15):
            self.send_data(self.lut_w[count])

        self.send_cmd(0x22)         #g bw r
        for count in range(15):
            self.send_data(self.lut_b[count])

        self.send_cmd(0x23)         #g wb w
        for count in range(15):
            self.send_data(self.lut_g1[count])

        self.send_cmd(0x24)         #g bb b
        for count in range(15):
            self.send_data(self.lut_g2[count])

    def set_lut_red(self):
        self.send_cmd(0x25)
        for count in range(15):
            self.send_data(self.lut_vcom1[count])

        self.send_cmd(0x26)
        for count in range(15):
            self.send_data(self.lut_red0[count])

        self.send_cmd(0x27)
        for count in range(15):
            self.send_data(self.lut_red1[count])

    def epd_reset(self):
        self.res.value = 0
        time.sleep(0.2)
        self.res.value = 1
        time.sleep(0.2)

    def refresh(self):
        b_b = self.pfb.buf_black
        b_r = self.pfb.buf_red

        self.send_cmd(self.DATA_START_TRANSMISSION_1)
        time.sleep(0.2)
        for i in range(self.height * self.width / 8):
            self.send_data(self.out_lut1[b_b[i]])
            self.send_data(self.out_lut2[b_b[i]])
        time.sleep(0.2)

        self.send_cmd(self.DATA_START_TRANSMISSION_2)
        time.sleep(0.2)
        for i in range(self.height * self.width / 8):
            self.send_data(b_r[i])
        time.sleep(0.2)

        self.send_cmd(self.DISPLAY_REFRESH)
        self.wait_until_idle()

    def epd_init(self):
        self.epd_reset()
        self.send_cmd(self.POWER_SETTING)
        self.send_data(0x07)
        self.send_data(0x00)
        self.send_data(0x08)
        self.send_data(0x00)
        self.send_cmd(self.BOOSTER_SOFT_START)
        self.send_data(0x07)
        self.send_data(0x07)
        self.send_data(0x07)
        self.send_cmd(self.POWER_ON)
        self.wait_until_idle()

        self.send_cmd(self.PANEL_SETTING)
        self.send_data(0xcf)
        self.send_cmd(self.VCOM_AND_DATA_INTERVAL_SETTING)
        self.send_data(0x17)
        self.send_cmd(self.PLL_CONTROL)
        self.send_data(0x39)
        self.send_cmd(self.TCON_RESOLUTION)
        self.send_data(0xC8)
        self.send_data(0x00)
        self.send_data(0xC8)
        self.send_cmd(self.VCM_DC_SETTING_REGISTER)
        self.send_data(0x30)

        self.set_lut_bw()
        self.set_lut_red()

        time.sleep(0.2)

class PapyrFb:
    """PapyrFb framebuffer object
    """
    def __init__(self):
        # pylint: disable=too-many-arguments
        self.width = 200
        self.height = 200
        self._rotation = 0
        self.buf_black = bytearray(round(self.width * self.height / 8))
        self.buf_red = bytearray(round(self.width * self.height / 8))
        self._font = None

    @property
    def rotation(self):
        """The rotation setting of the display, can be one of (0, 1, 2, 3)"""
        return self._rotation

    @rotation.setter
    def rotation(self, val):
        if not val in (0, 1, 2, 3):
            raise RuntimeError("Bad rotation setting")
        self._rotation = val

    def fill(self, color):
        """Fill the entire FrameBuffer with the specified color."""
        if color == WHITE:
            for i in range(len(self.buf_black)):
                self.buf_black[i] = 0xFF
                self.buf_red[i] = 0xFF
        elif color == BLACK:
            for i in range(len(self.buf_black)):
                self.buf_black[i] = 0x00
                self.buf_red[i] = 0xFF
        elif color == RED:
            for i in range(len(self.buf_black)):
                self.buf_black[i] = 0xFF
                self.buf_red[i] = 0x00

    def fill_rect(self, x, y, width, height, color):
        """Draw a rectangle at the given location, size and color. The ``fill_rect`` method draws
        both the outline and interior."""
        # pylint: disable=too-many-arguments, too-many-boolean-expressions
        self.rect(x, y, width, height, color, fill=True)

    def pixel(self, x, y, color=None):
        """If ``color`` is not given, get the color value of the specified pixel. If ``color`` is
        given, set the specified pixel to the given color."""
        if self.rotation == 1:
            x, y = y, x
            x = self.width - x - 1
        if self.rotation == 2:
            x = self.width - x - 1
            y = self.height - y - 1
        if self.rotation == 3:
            x, y = y, x
            y = self.height - y - 1

        if x < 0 or x >= self.width or y < 0 or y >= self.height:
            return None
        if color is None:
            return self.get_pixel(self, x, y)
        self._set_pixel(x, y, color)
        return None

    def hline(self, x, y, width, color):
        """Draw a horizontal line up to a given length."""
        self.line(x, y, x + width, y, color)

    def vline(self, x, y, height, color):
        """Draw a vertical line up to a given length."""
        self.line(x, y, x, y + height, color)

    def rect(self, x, y, width, height, color, *, fill=False):
        """Draw a rectangle at the given location, size and color. The ```rect``` method draws only
        a 1 pixel outline."""
        # pylint: disable=too-many-arguments
        if self.rotation == 1:
            x, y = y, x
            width, height = height, width
            x = self.width - x - width
        if self.rotation == 2:
            x = self.width - x - width
            y = self.height - y - height
        if self.rotation == 3:
            x, y = y, x
            width, height = height, width
            y = self.height - y - height

        # pylint: disable=too-many-boolean-expressions
        if width < 1 or height < 1 or (x + width) <= 0 or (y + height) <= 0 or \
                y >= self.height or x >= self.width:
            return
        x_end = min(self.width-1, x + width-1)
        y_end = min(self.height-1, y + height-1)
        x = max(x, 0)
        y = max(y, 0)
        if fill:
            self._fill_rect(self, x, y, x_end-x+1, y_end-y+1, color)
        else:
            self.hline(x, y, x_end-x+1, color)
            self.hline(x, y_end+1, x_end-x+1, color)
            self.vline(x, y, y_end-y+1, color)
            self.vline(x_end+1, y, y_end-y+1, color)

    def line(self, x_0, y_0, x_1, y_1, color):
        # pylint: disable=too-many-arguments
        """Bresenham's line algorithm"""
        d_x = abs(x_1 - x_0)
        d_y = abs(y_1 - y_0)
        x, y = x_0, y_0
        s_x = -1 if x_0 > x_1 else 1
        s_y = -1 if y_0 > y_1 else 1
        if d_x > d_y:
            err = d_x / 2.0
            while x != x_1:
                self.pixel(x, y, color)
                err -= d_y
                if err < 0:
                    y += s_y
                    err += d_x
                x += s_x
        else:
            err = d_y / 2.0
            while y != y_1:
                self.pixel(x, y, color)
                err -= d_x
                if err < 0:
                    x += s_x
                    err += d_y
                y += s_y
        self.pixel(x, y, color)

    def circle(self, x, y, radius, color):
        # Bresenham algorithm
        x_pos = -radius
        y_pos = 0
        err = 2 - 2 * radius
        e2 = 0

        while True:
            self.pixel(x - x_pos, y + y_pos, color)
            self.pixel(x + x_pos, y + y_pos, color)
            self.pixel(x + x_pos, y - y_pos, color)
            self.pixel(x - x_pos, y - y_pos, color)
            e2 = err
            if (e2 <= y_pos):
                y_pos = y_pos + 1
                err = err + y_pos * 2 + 1
                if(-x_pos == y_pos and e2 <= x_pos):
                  e2 = 0

            if (e2 > x_pos):
                x_pos = x_pos + 1
                err = err + x_pos * 2 + 1

            if x_pos > 0:
                break

    def blit(self):
        """blit is not yet implemented"""
        raise NotImplementedError()

    def scroll(self, delta_x, delta_y):
        """shifts framebuf in x and y direction"""
        if delta_x < 0:
            shift_x = 0
            xend = self.width + delta_x
            dt_x = 1
        else:
            shift_x = self.width - 1
            xend = delta_x - 1
            dt_x = -1
        if delta_y < 0:
            y = 0
            yend = self.height + delta_y
            dt_y = 1
        else:
            y = self.height - 1
            yend = delta_y - 1
            dt_y = -1
        while y != yend:
            x = shift_x
            while x != xend:
                _set_pixel(
                    x, y, _format.get_pixel(self, x - delta_x, y - delta_y))
                x += dt_x
            y += dt_y

    def text(self, string, x, y, color, *,
             font_name="font5x8.bin"):
        """text is not yet implemented"""
        if not self._font or self._font.font_name != font_name:
            # load the font!
            self._font = BitmapFont()
        w = self._font.font_width
        for i, char in enumerate(string):
            self._font.draw_char(char,
                                 x + (i * (w + 1)),
                                 y, self, color)

    def image(self, img):
        """Set buffer to value of Python Imaging Library image.  The image should
        be in 1 bit mode and a size equal to the display size."""
        if img.mode != '1':
            raise ValueError('Image must be in mode 1.')
        imwidth, imheight = img.size
        if imwidth != self.width or imheight != self.height:
            raise ValueError('Image must be same dimensions as display ({0}x{1}).' \
                .format(self.width, self.height))
        # Grab all the pixels from the image, faster than getpixel.
        pixels = img.load()
        # Clear buffer
        for i in range(len(self.buf)):
            self.buf[i] = 0
        # Iterate through the pixels
        for x in range(self.width):       # yes this double loop is slow,
            for y in range(self.height):  #  but these displays are small!
                if pixels[(x, y)]:
                    self.pixel(x, y, 1)   # only write if pixel is true

    def _set_pixel(self, x, y, color):
        """Set a given pixel to a color."""
        if color == WHITE:
            self.buf_black[int((x + y * self.width) / 8)] &= ~(0x80 >> (x % 8))
            self.buf_red[int((x + y * self.width) / 8)] &= ~(0x80 >> (x % 8))
        elif color == BLACK:
            self.buf_red[int((x + y * self.width) / 8)] |= 0x80 >> (x % 8)
            self.buf_black[int((x + y * self.width) / 8)] &= ~(0x80 >> (x % 8))
        elif color == RED:
            self.buf_red[int((x + y * self.width) / 8)] &= ~(0x80 >> (x % 8))
            self.buf_black[int((x + y * self.width) / 8)] |= 0x80 >> (x % 8)

    @staticmethod
    def get_pixel(self, x, y):
        """Get the color of a given pixel"""
        black_pixel = self.buf_black[int((x + y * self.width) / 8)] & (0x80 >> (x % 8))
        red_pixel = self.buf_red[int((x + y * self.width) / 8)] & (0x80 >> (x % 8))

        if black_pixel == 0: # black set
            return BLACK
        elif red_pixel == 0:
            return RED
        else:
            return WHITE

    @staticmethod
    def _fill_rect(framebuf, x, y, width, height, color):
        """Draw a rectangle at the given location, size and color. The ``fill_rect`` method draws
        both the outline and interior.
        TODO
        """



# MicroPython basic bitmap font renderer.
# Author: Tony DiCola
# License: MIT License (https://opensource.org/licenses/MIT)
class BitmapFont:
    """A helper class to read binary font tiles and 'seek' through them as a
    file to display in a framebuffer. We use file access so we dont waste 1KB
    of RAM on a font!"""
    def __init__(self, font_name='font5x8.bin'):
        # Specify the drawing area width and height, and the pixel function to
        # call when drawing pixels (should take an x and y param at least).
        # Optionally specify font_name to override the font file to use (default
        # is font5x8.bin).  The font format is a binary file with the following
        # format:
        # - 1 unsigned byte: font character width in pixels
        # - 1 unsigned byte: font character height in pixels
        # - x bytes: font data, in ASCII order covering all 255 characters.
        #            Each character should have a byte for each pixel column of
        #            data (i.e. a 5x8 font has 5 bytes per character).
        self.font_name = font_name

        # Open the font file and grab the character width and height values.
        # Note that only fonts up to 8 pixels tall are currently supported.
        try:
            self._font = open(self.font_name, 'rb')
        except OSError:
            print("Could not find font file", font_name)
            raise
        self.font_width, self.font_height = struct.unpack('BB', self._font.read(2))

    def deinit(self):
        """Close the font file as cleanup."""
        self._font.close()

    def __enter__(self):
        """Initialize/open the font file"""
        self.__init__()
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        """cleanup on exit"""
        self.deinit()

    def draw_char(self, char, x, y, framebuffer, color):
        # pylint: disable=too-many-arguments
        """Draw one character at position (x,y) to a framebuffer in a given color"""
        # Don't draw the character if it will be clipped off the visible area.
        #if x < -self.font_width or x >= framebuffer.width or \
        #   y < -self.font_height or y >= framebuffer.height:
        #    return
        # Go through each column of the character.
        for char_x in range(self.font_width):
            # Grab the byte for the current column of font data.
            self._font.seek(2 + (ord(char) * self.font_width) + char_x)
            try:
                line = struct.unpack('B', self._font.read(1))[0]
            except RuntimeError:
                continue # maybe character isnt there? go to next
            # Go through each row in the column byte.
            for char_y in range(self.font_height):
                # Draw a pixel for each bit that's flipped on.
                if (line >> char_y) & 0x1:
                    framebuffer.pixel(x + char_x, y + char_y, color)

    def width(self, text):
        """Return the pixel width of the specified text message."""
        return len(text) * (self.font_width + 1)