## Papyr testing

### Test LED
1. Flash `test-led.hex` firmware, **Detailed instructions** -> https://docs.electronut.in/papyr/programming_guide/#upload-hex-file
2. Power the papyr board.
3. LED will flash in sequence GREEN(GPIO 13), BLUE(GPIO 14), RED(GPIO 15).

### Test USB
1. Flash `test-usb.hex` firmware, **Detailed instructions** -> https://docs.electronut.in/papyr/programming_guide/#upload-hex-file
2. Connect Papyr USB device to host machine.

**On linux:**
* Run command `dmesg -wH`
* Reset the board and observe the output, you should see the following **highlighted** text.

![](images/linux-papyr-usb-test.png)

**On Windows:**
* Open device manager
* You will see the following if it's working correctly.

![](images/windows-papyr-usb-test.png)

### Test NFC
1. Flash `test-nfc.hex` firmware, **Detailed instructions** -> https://docs.electronut.in/papyr/programming_guide/#upload-hex-file
2. Power papyr board with coin cell
3. Touch the NFC antenna with the smartphone or tablet and observe text on display.
4. You should see this text.

<img src="./images/test-nfc.jpg" width="400"/>