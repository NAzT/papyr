# MQTT specifix library
import paho.mqtt.client as mqtt # Installed via: pip install paho-mqtt

# Data specific libraries
import wget
import requests
import subprocess
import os

# Broker and Topic Deta
MQTT_BROKER_HOST_ADDRESS = 'broker.mqttdashboard.com'
MQTT_BROKER_HOST_PORT = 1883
MQTT_TOPIC = 'electronut/BGIFD/base64' # IMP: Update this before running


def getWeather():
    # Get your location
    response = requests.get('http://ipinfo.io/json')
    locData = response.json()
    city = locData['city']

    print city

    # Download current weather as image
    url = 'http://wttr.in/' + city + '_0qT.png'
    fileName = wget.download(url)
    print

    # Convert weather image to 200x200px
    subprocess.call('convert -background black -gravity center ' + fileName + ' -resize 200x200 -extent 200x200 result.png', shell=True)

    with open('result.png', "rb") as f:
        data = f.read()
        imageDataToSend = 'data:image/png;base64,' + data.encode("base64")
    
    # delete intermediate files
    os.remove(fileName)
    os.remove('result.png')

    return imageDataToSend


# Create client and connect to it
client = mqtt.Client()
client.connect(MQTT_BROKER_HOST_ADDRESS, MQTT_BROKER_HOST_PORT)

# Generate payload in BASE64 format
payload = getWeather()

# Publish data from broker to clients
client.publish(MQTT_TOPIC, payload)
