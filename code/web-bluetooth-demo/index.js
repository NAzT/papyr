/** index.js
 *
 * brief: Web Bluetooth demo code for papyr for docs. 
 *
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
 */
var button = document.getElementById('connect');
var send = document.getElementById('send');
var papyr_char;
var img = new Image();
var ctx, canvas;
var ctx2, canvas2;
var saveX, saveY, selectedBg;
var selectedColor = 'black';
var bluetoothDevice;
var status;
var moved = function moved(ev) { };

img.crossOrigin = "anonymous";
send.disabled = true;
send.setAttribute("data-md-color-primary", "grey");
canvas = document.getElementById('canvas1');
canvas2 = document.getElementById('canvas2');
canvas.width = canvas.height = 0;
document.body.style.overflow = "inherit";

/* LoadCanvas()
* Initates at window load, canvas width based on content width is set.
* Another hidden canvas is initialised and both are set as white background.
* Also, browser bluetooth detection is done, and send button is grayed out
*/ 
function LoadCanvas() {
    if (!navigator.bluetooth) {
        document.getElementById('status_p').innerText = "This browser is not supported";
        document.getElementById('status_p').style.color = "red";
    }

    send.disabled = true;
    send.setAttribute("data-md-color-primary", "grey");

    if (document.getElementsByTagName("article")[0].offsetWidth < 400) {
        canvas.height = canvas.width = document.getElementsByTagName("article")[0].offsetWidth * 0.85;
    } else {
        canvas.height = canvas.width = document.getElementsByTagName("article")[0].offsetWidth * 0.70;
    }

    ctx = canvas.getContext("2d");
    ctx2 = canvas2.getContext("2d");
    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, canvas.width, canvas.height)
    ctx2.fillStyle = "white";
    ctx2.fillRect(0, 0, canvas2.width, canvas2.height)

};


function RLECompress(input) {
    var count = 0,
        index, i, pixel, out = 0;
    length = input.length;
    var output = [];

    while (count < length) {
        index = count;
        pixel = input[index++];
        while (index < length && index - count < 127 && input[index] == pixel) {
            index++;
        }
        if ((index - count) == 1) {
            /* 
               Failed to "replicate" the current pixel. See how many to copy.
               Avoid a replicate run of only 2-pixels after a literal run. There
               is no gain in this, and there is a risK of loss if the run after
               the two identical pixels is another literal run. So search for
               3 identical pixels.
            */
            while (index < length && index - count < 127 &&
                (input[index] != input[index - 1] ||
                    index > 1 && input[index] != input[index - 2])) {
                index++;
            }
            /* 
               Check why this run stopped. If it found two identical pixels, reset
               the index so we can add a run. Do this twice: the previous run
               tried to detect a replicate run of at least 3 pixels. So we may be
               able to back up two pixels if such a replicate run was found.
            */
            while (index < length && input[index] == input[index - 1]) {
                index--;
            }
            output[out++] = (count - index);
            for (i = count; i < index; i++) {
                output[out++] = input[i];
            }
        } else {
            output[out++] = (index - count);
            output[out++] = pixel;
        } /* if */
        count = index;
    } /* while */
    return new Uint8Array(output);
}

window.onload = function () {
    LoadCanvas();
}

/* Click bind to send button which converts the bigger canvas to 200 x 200 canvas and extracts every pixel data.
*  Black and Red pixels are taken out seperately, and are then combines to create a big array containing all bits in black and red.
*  RLE Compression is used and data is sent over BLE with chunks of 16 byte data and 2 byte frame number.
*/
send.addEventListener('click', async function (event) {

    var dataUrl = canvas.toDataURL();
    var img_new = new Image();
    img_new.src = dataUrl;

    img_new.onload = async function () {
        await ctx2.drawImage(img_new, 0, 0, 200, 200);

        var bwimg = [];
        var redImg = [];
        for (let i = 200; i > 0; --i) {
            for (let j = 0; j < 200; j++) {
                var pixel = ctx2.getImageData(i, j, 1, 1);
                if (pixel.data[0] < 100 && pixel.data[1] < 100 && pixel.data[2] < 100) {
                    bwimg.push(1)
                } else {
                    bwimg.push(0)
                }

                if (pixel.data[0] > (255 - 100) && pixel.data[1] < 100 && pixel.data[2] < 100) {
                    redImg.push(1);
                } else {
                    redImg.push(0);
                }
            }
        }

        bwimg = bwimg.concat(redImg);

        function decimalToHexString(number) {
            if (number < 0) {
                number = 0xFF + number + 1;
            }
            return '0x' + number.toString(16).toUpperCase();
        }

        var length5kset = [];

        for (var i = 0; i < bwimg.length; i += 8) {
            var a = 0xff;
            for (var j = 0; j < 8; j++) {
                a = a & ~((0x1 & bwimg[i + j]) << 7 - j);
            }
            await length5kset.push(decimalToHexString(a));
        }

        var compressedImage = Array.from(RLECompress(length5kset));
        var frame_no = 1;
        var data_length = 16;

        while (compressedImage.length != 0) {

            var image_data_frame = compressedImage.splice(0, data_length)
            var vnd_value = [];

            if (image_data_frame.length < data_length || (image_data_frame.length == data_length && compressedImage.length == 0)) {
                vnd_value.push(0) // Last frame prefix:  "0b 0000 0000"
                vnd_value.push(0)
            } else {
                vnd_value.push((frame_no & 0xff00) >> 8)
                vnd_value.push(frame_no & 0x00ff)
            }

            for (let x = 0; x < image_data_frame.length; x++) {
                vnd_value.push(image_data_frame[x]);
            }

            const vnd_value_buf = new Uint8Array(vnd_value);

            await papyr_char.writeValue(vnd_value_buf)
                .then(a => { });

            frame_no++;

        }
    }

});

/* Click bind to connect button which trigger connect/disconnect button. Connect executes web bluetooth connection script
*  device connection provides a gatt server and then services and characterstic data is taken out of it. Similarly, 
*  disconnection requires gatt server disconnect method.
*/
button.addEventListener('click', async function (event) {
    if (button.innerText == "Connect") {
        navigator.bluetooth.requestDevice({
            filters: [{
                name: 'Papyr'
            }],
            optionalServices: ['7f3a1400-32b7-4de8-9fb0-92ef46108113']
        })
        .then(device => {
            bluetoothDevice = device;
            bluetoothDevice.addEventListener('gattserverdisconnected', onDisconnected);
            console.log(device.gatt);
            return device.gatt.connect();
        })
        .then(server => {
            console.log(server);
            return server.getPrimaryService("7f3a1400-32b7-4de8-9fb0-92ef46108113");
        })
        .then(service => {
            console.log('Getting Characteristic...');
            return service.getCharacteristic("7f3a1401-32b7-4de8-9fb0-92ef46108113");
        })
        .then(characteristic => {
            console.log(characteristic);
            papyr_char = characteristic;
            send.disabled = false;
            send.setAttribute("data-md-color-primary", "light-blue");
            // LoadCanvas();
            button.innerText = "Disconnect"

        })
        .catch(error => {
            console.log(error);
        });
    } else {
        if (!bluetoothDevice) {
            return;
        }
        console.log('Disconnecting from Bluetooth Device...');
        if (bluetoothDevice.gatt.connected) {
            bluetoothDevice.gatt.disconnect();
        } else {
            console.log('> Bluetooth Device is already disconnected');
        }
    }
});

function onDisconnected(event) {
    // Object event.target is Bluetooth Device getting disconnected.
    send.disabled = true;
    send.setAttribute("data-md-color-primary", "grey");
    console.log('> Bluetooth Device disconnected');
    button.innerText = "Connect"
}

// Pen color for drawing on canvas
function selectColor(color) {
    selectedColor = color;
}

// Background color for drawing on canvas
function selectbgColor(color) {
    selectedBg = color;
    ctx.fillStyle = color;
    ctx.fillRect(0, 0, canvas.width, canvas.height)
}


function stopDrawing(e) {
    document.body.style.overflow = "scroll";
    moved = function (ev) { }
}

function startDrawing(ev) {
    document.body.style.overflow = "hidden";

    var canvasPosition = canvas.getBoundingClientRect();
    if (!ev.touches) {
        saveX = ev.pageX - canvasPosition.x;
        saveY = ev.pageY - canvasPosition.y;
    } else {
        saveX = ev.touches[0].pageX - canvasPosition.x;
        saveY = ev.touches[0].pageY - canvasPosition.y;
    }
    /* Moved method is actually used to draw on the canvas, on mouse up, stop drawing is executed and moved function is empty assigned
    *  But when user starts drawing, we use the current co-ordinates to capture the current position and every movement point on the 
    *  canvas. Canvas initial point on page, is taken , and current position of mouse is taken and then is calculated.
    */ 
   moved = function (ev) {
        let currentX, currentY;

        var canvasPosition = canvas.getBoundingClientRect();
        if (!ev.touches) {
            currentX = ev.pageX - canvasPosition.x;
            currentY = ev.pageY - canvasPosition.y;
        } else {
            currentX = ev.touches[0].pageX - canvasPosition.x;
            currentY = ev.touches[0].pageY - canvasPosition.y;
        }

        ctx.lineJoin = 'round';
        ctx.strokeStyle = selectedColor;
        ctx.lineWidth = 8;
        ctx.beginPath();
        ctx.moveTo(saveX, saveY);
        ctx.lineTo(currentX, currentY);
        ctx.closePath();
        ctx.stroke();

        saveX = currentX;
        saveY = currentY;
    }
}